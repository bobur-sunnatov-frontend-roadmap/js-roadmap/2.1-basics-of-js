// Task.1 GIVEN A NUMBER  `X`, RETURN  `TRUE`  IF  `X`  IS PALINDROME NUMBER.

function isPalindrome(x) {
    //negative numbers cannot be palindrome numbers
    if(x<0) {
        return false
    }
    //palindromic number cannot be divided by 10 
    if(x%10==0) {
        return false
    }
    //0 is not palindromic 
    if(x==0) {
        return false
    }
    let str=String(x);
    let i = 0; 
    let j = str.length - 1;
    while (i < j) {
        if (str[i]!=str[j]) {
        return false;
        }
        else  return true
        i++;
        j--;
    }
}
console.log(isPalindrome(111));



// Task 2. GIVEN AN ARRAY `ARR`, REMOVE THE DUPLICATES SUCH THAT EACH UNIQUE ELEMENT APPEARS ONLY **ONCE**. 
function removeDuplicates(arr) {
    for (let i=0; i<arr.length; i++) {
        for (let j=0; j<arr.length-1; j++){
            if (arr[i]==arr[j] && i!==j){
                arr.splice(i,1)
            }
        }
    }
    return arr
}
console.log(removeDuplicates([1, 1, 2, 2, 2, 3, 3, 3, 4]));


// Task 3. GIVEN A SORTED ARRAY OF DISTINCT NUMBERS AND A TARGET VALUE, RETURN THE INDEX IF THE TARGET IS FOUND. IF NOT, RETURN THE INDEX WHERE IT WOULD BE IF IT WERE INSERTED IN AORDER

function targetSearch(nums, target) {
    let i=0;
    let notFound=false;
    while (!notFound) {
        if (nums[i] === target) {
            notFound = true;
            return i;}
        else if (nums[i]<target)
                i++
            else {
                nums.splice(i,0,target)
                notFound = true;
                return nums.indexOf(target) }
        }
    }
    
console.log(targetSearch([1,3,5,6], 2))
